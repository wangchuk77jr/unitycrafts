import Home from './guest/Home';
import Shop from './guest/Shop';
import About from './guest/About';
import ContactUs from './guest/ContactUs';
import ViewShopDetails from './guest/ViewShopDetails';
import ProductCategory from './guest/ProductCategory';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Profile from './guest/Profile';
import Cart from './guest/Cart';



function App() {
  return (
    <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/shop" element={<Shop />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/cart" element={<Cart />} />

          <Route path="/about" element={<About />} />
          <Route path='/contactUs' element={<ContactUs/>}/>
          <Route path='/shopdetails' element={<ViewShopDetails/>}/>
          <Route path='/productCategory' element={<ProductCategory/>}/>

        </Routes>
    </Router>
  );
}

export default App;
