import React, { useState, useEffect } from 'react';
import slider01 from '../images/slider-01.png';
import slider02 from '../images/slider-02.png';
import slider03 from '../images/slider-03.png';

const HeroBannerSlider = () => {
  const images = [slider01, slider02, slider03];
  const totalSlides = images.length;

  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    const autoplayInterval = setInterval(() => {
      nextSlide();
    }, 3000);

    return () => clearInterval(autoplayInterval);
  }, [currentSlide]);

  const nextSlide = () => {
    setCurrentSlide((currentSlide + 1) % totalSlides);
  };

  const prevSlide = () => {
    setCurrentSlide((currentSlide - 1 + totalSlides) % totalSlides);
  };

  return (
    <div style={{ height: '70vh' }}>
      <div className='heroBanner-slider'>
        <img className='slider-image' src={images[currentSlide]} alt={`slider-0${currentSlide + 1}`} />
      </div>
      <div className='container mt-5 w-100 px-5 h-100 d-flex align-items-center gap-5'>
        <div className="">
          <div className='line' style={{ height: '200px', width: '8px', background: '#fff', borderRadius: '2px' }}></div>
        </div>
        <div>
          <h1 className='slider-content'>Fostering connections through culture, commerce, and sustainability —one click at a time</h1>
          <button className='mt-4 btn btn-primary shopnowBtn'>Shop Now</button>
        </div>
        <div className='ms-auto d-flex flex-column gap-2'>
          {images.map((_, index) => (
            <button key={index} type='button' className={`slider-indicator p-0 m-0 ${index === currentSlide ? 'indicator-active' : ''}`} onClick={() => setCurrentSlide(index)}></button>
          ))}
        </div>
      </div>
      <div className='buttonNextPrev w-100'>
        <button className="prev pt-2 px-3" onClick={prevSlide}>
          <box-icon size='lg' name='play-circle' rotate='180' color='#996418' ></box-icon>
        </button>
        <button className="next pt-2 px-3" onClick={nextSlide}>
          <box-icon size="lg" name='play-circle' color='#996418' ></box-icon>
        </button>
      </div>
    </div>
  );
}

export default HeroBannerSlider;
