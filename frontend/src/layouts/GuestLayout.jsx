import 'boxicons'
import '../css/GuestLayout.css';
import React, { useState, useEffect } from 'react';
import { NavLink,Link } from 'react-router-dom'
import logo from '../images/logo.png';
import { useLocation } from 'react-router-dom';
import HeroBannerSlider from './HeroBannerSlider';

const GuestLayout = ({children}) => {
    const location = useLocation();
    const isHomePage = location.pathname === '/';
    const additionalClass = isHomePage ? 'homeNavbar' : 'generalNavbar';
    const [scrolling, setScrolling] = useState(false);

    useEffect(() => {
      const handleScroll = () => {
        if (window.scrollY > 50) {
          setScrolling(true);
        } else {
          setScrolling(false);
        }
      };
  
      window.addEventListener('scroll', handleScroll);
  
      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }, []);
  return (
    <div>
       <header className={isHomePage ? 'position-relative vh-100' : ''}>
        <nav className={`navbar navbar-expand-lg position-sticky top-0 px-md-5 ${additionalClass} ${scrolling ? 'bg-secondary border-bottom-0' : ''}`}>
          <div className="container-fluid">
            <NavLink className="navbar-brand me-md-5" to="/">
              <img src={logo} alt="logo" />
            </NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <box-icon name='menu' color='#ffffff' ></box-icon>            
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav w-100 gap-3 ms-md-5 mb-2 mb-lg-0">
                    <li className="nav-item">
                        <NavLink className="nav-link text-white text-uppercase" aria-current="page" to="/">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link text-white text-uppercase" to="/shop">Shop</NavLink>
                    </li>
                    <li className="nav-item dropdown">
                        <Link className="nav-link dropdown-toggle text-white text-uppercase" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Product Category
                        </Link>
                        <ul className="dropdown-menu bg-dark mt-3">
                            <div className='product-category p-3'>
                                <li><Link className="dropdown-item-nav" to="/productCategory">Thangka Painting</Link></li>
                                <li><Link className="dropdown-item-nav" to="/productCategory">Woodwork</Link></li>
                                <li><Link className="dropdown-item-nav" to="/productCategory">Bamboo and Cane Craft</Link></li>
                                <li><Link className="dropdown-item-nav" to="/productCategory">Thangka Painting</Link></li>
                                <li><Link className="dropdown-item-nav" to="/productCategory">Woodwork</Link></li>
                                <li><Link className="dropdown-item-nav" to="/productCategory">Bamboo and Cane Craft</Link></li>
                            </div>
                        </ul>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link text-white text-uppercase" to="/contactUs">Contact</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link text-white text-uppercase" to="/about">About</NavLink>
                    </li>
                    <li className='nav-item  ms-md-auto '>
                        <Link className='nav-link'>
                            <box-icon name='search' color='#fff'></box-icon>
                        </Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to="/cart">
                            <box-icon name='cart' color='#fff'></box-icon>
                        </Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to="/profile">
                            <box-icon name='user' color='#fff'></box-icon>
                        </Link>
                    </li>
                </ul>
            </div>
          </div>
        </nav>
        {isHomePage && <HeroBannerSlider />} 
       </header> 

        <main className='container-fluid'>{children}</main>

        <footer style={{minHeight:'40vh'}} className='bg-black pb-3'>
            <div className="container-fluid px-md-5">
              <div className="row pt-5">
                <div className="col-md-5">
                  <img src={logo} alt="logo" />
                  <p className='footer-text text-white text-opacity-50 pe-md-4 mt-3'>
                    For Bhutanese patrons, UnityCrafts simplifies local shopping.
                    With a normal account, explore, order, and enjoy high-quality
                    products effortlessly. Join us in fostering a thriving local
                    economy with just a few clicks!
                  </p>
                </div>
                <div className="col-md-3">
                  <h5 style={{fontWeight:'700',}} className='text-white text-uppercase'>Quick Links</h5>
                  <ul className='list-unstyled mt-4 pt-md-3 d-flex flex-column gap-2'>
                    <li>
                      <Link className='footer-text text-decoration-none text-white text-opacity-50'>Create Bussiness Account</Link>
                    </li>
                    <li>
                      <Link className='footer-text text-decoration-none text-white text-opacity-50'>Create Customer Account</Link>
                    </li>
                    <li>
                      <Link className='footer-text text-decoration-none text-white text-opacity-50'>Contact Us</Link>
                    </li>
                    <li>
                      <Link className='footer-text text-decoration-none text-white text-opacity-50'>About Us</Link>
                    </li>
                  </ul>
                </div>
                <div className="col-md-4">
                  <h6 className='text-white text-uppercase mt-md-5'>Follow us in social Media</h6>
                  <div className="social-media d-flex gap-3 mt-4">
                     <Link className='social-icons'><box-icon size='md' type='logo' name='facebook'></box-icon></Link>
                     <Link className='social-icons'><box-icon size='md' name='instagram' type='logo' ></box-icon></Link>
                     <Link className='social-icons'><box-icon size='md' name='tiktok' type='logo' ></box-icon></Link>
                     <Link className='social-icons'><box-icon size='md' name='youtube' type='logo' ></box-icon></Link>
                  </div>
                </div>
              </div>
              <div className="row d-flex flex-column align-items-center gap-3 mt-4">
                <span className='line text-center'></span>
                <span style={{fontSize:'14px'}} className='text-center text-white opacity-50' >© 2024 UnityCrafts.com. All rights reserved.</span>
              </div>
            </div>
        </footer>

    </div>
  )
}

export default GuestLayout