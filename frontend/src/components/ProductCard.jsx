import React from 'react';
import '../css/ProductCard.css';

const ProductCard = ({...props}) => {
  return (
    <div className="col-md-3">
      <div className='product-card'>
        <div className="productImg">
          <img src={props.productImg} alt="{props.productName}" />
        </div>
        <div className="product-details p-3">
          <div className='d-flex justify-content-between'>
            <h5 className='product-title'>{props.productName}</h5>
            <p className='product-owner'>-By {props.sellerName}</p>
          </div>
          <p style={{ WebkitLineClamp: 3, overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', '-webkit-box-orient': 'vertical' }} className='productDes'>
            {props.productDes}
          </p>
          <div className="price-status d-flex justify-content-between">
            <h5 className='price'>NU. {props.productPrice}</h5>
            <p className='d-flex'>
              <box-icon size='xs' type='solid' color='#05FF00' name='circle'></box-icon>
              <span className='status ps-2'>Available</span>
            </p>
          </div>
          <div className="product-actions d-flex justify-content-between gap-3">
            <button className='btn-add-to-cart text-white w-75'>Add to cart</button>
            <button className='btn-comment'><box-icon name='message-rounded-dots' color='#996418' ></box-icon></button>
            <div className='total-products px-2'>{props.prouductTotal}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
