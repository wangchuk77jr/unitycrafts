import React from "react";
import GuestLayout from "../layouts/GuestLayout";
import "./../css/profile.css";
import img from "./../images/profile.png";

const Profile = () => {
    return (
        <GuestLayout>
            <div className="outerC">
                <div className="c11">
                    <h3 style={{ marginTop: "40px",marginBottom:"40px" }}>ACCOUNT DETAILS</h3>
                    <form action="#" method="post">
                        <div className="form-group">
                            <label htmlFor="photo">Upload Photo</label>
                            <input type="file" id="photo" name="photo"  />
                        </div>

                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input type="text" id="name" name="name" placeholder="Your Name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Phone No</label>
                            <input type="text" id="phone" name="phone" placeholder="Your Phone Number" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="location">Location</label>
                            <input type="text" id="location" name="location" placeholder="Your Location" value="Location" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="old-password">Old Password</label>
                            <input type="password" id="old-password" name="old-password" placeholder="Your Old Password" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="new-password">New Password</label>
                            <input type="password" id="new-password" name="new-password" placeholder="Your New Password" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="confirm-password">Confirm Password</label>
                            <input type="password" id="confirm-password" name="confirm-password" placeholder="Confirm New Password" />
                        </div>
                        <button style={{ marginLeft: "10%" }} type="submit">UPDATE</button>
                    </form>
                </div>
                <div className="c22">
                    <div className="d1">
                        <div className="d2">
                            <div className="d3">
                                <img src={img} alt="img" />
                            </div>
                        </div>
                    </div>
                    <button style={{ width: "380px",marginTop:"40px" }} type="submit">LOGOUT</button>
                </div>
            </div>
        </GuestLayout>
    );
};

export default Profile;
