import React from 'react'
import GuestLayout from '../layouts/GuestLayout'
import "./../css/Shop.css"
import { Link } from 'react-router-dom'

const Shop = () => {
  return (
    <GuestLayout>
      <div className='row bg-danger shopHero-banner'>
        <h1 className='text-center' style={{ color: "white", fontWeight: "bold" }}>Shops</h1>
      </div>
      <div className='mb-5' style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}}>
        <div className='each'>
          <div className='c1'>
            <div className='c3'>
              <img className='imm' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
              <p style={{ paddingTop: "5px" }}>Tenzin Om Tshongkhang</p>
              <p>Location : Punakha</p>
              <div style={{ backgroundcolor: "#AD854D" }}>
                <Link  to="/shopdetails" className="btn btn-primary d-block mx-auto">View Shop</Link>
              </div>
            </div>
          </div>
          <div className='c2'>
            {/* <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div>
          <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div> */}

          </div>
        </div>
        <div className='each'>
          <div className='c1'>
            <div className='c3'>
              <img className='imm' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
              <p style={{ paddingTop: "5px" }}>Tenzin Om Tshongkhang</p>
              <p>Location : Punakha</p>
              <div style={{ backgroundcolor: "#AD854D" }}>
                <button className='bbt btn btn-primary'>Visit Shop</button>
              </div>
            </div>
          </div>
          <div className='c2'>
            {/* <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div>
          <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div> */}

          </div>
        </div>
        <div className='each'>
          <div className='c1'>
            <div className='c3'>
              <img className='imm' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
              <p style={{ paddingTop: "5px" }}>Tenzin Om Tshongkhang</p>
              <p>Location : Punakha</p>
              <div style={{ backgroundcolor: "#AD854D" }}>
                <button className='bbt btn btn-primary btn btn-primary'>Visit Shop</button>
              </div>
            </div>
          </div>
          <div className='c2'>
            {/* <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div>
          <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div> */}

          </div>
        </div>
        <div className='each'>
          <div className='c1'>
            <div className='c3'>
              <img className='imm' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
              <p style={{ paddingTop: "5px" }}>Tenzin Om Tshongkhang</p>
              <p>Location : Punakha</p>
              <div style={{ backgroundcolor: "#AD854D" }}>
                <button className='bbt btn btn-primary'>Visit Shop</button>
              </div>
            </div>
          </div>
          <div className='c2'>
            {/* <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div>
          <div> */}
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            <img className='im' src="https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1" alt="img" />
            {/* </div> */}

          </div>
        </div>
      </div>
    </GuestLayout>
  )
}

export default Shop

