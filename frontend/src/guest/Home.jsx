import React from 'react';
import GuestLayout from '../layouts/GuestLayout';
import '../css/Home.css';
import ProductCard from '../components/ProductCard';
import { Link } from 'react-router-dom';

const Home = () => {
  const topSellingProducts = [
    { 
      id: 1,
      productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
      productName: 'Bangchung',
      sellername: 'Tenzin Om', 
      productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
      productPrice: 200,
      availiableStatus: 'available',
      prouductTotal: 18,
    },
    { 
      id: 2,
      productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
      productName: 'Bangchung',
      sellername: 'Tenzin Om', 
      productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
      productPrice: 200,
      availiableStatus: 'available',
      prouductTotal: 18,
    },
    { 
      id: 3,
      productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
      productName: 'Bangchung',
      sellername: 'Tenzin Om', 
      productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
      productPrice: 200,
      availiableStatus: 'available',
      prouductTotal: 18,
    },
    { 
      id: 4,
      productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
      productName: 'Bangchung',
      sellername: 'Tenzin Om', 
      productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
      productPrice: 200,
      availiableStatus: 'available',
      prouductTotal: 18,
    },
  ];

  return (
    <GuestLayout>
      {/* Top Selling Product Section */}
      <section className='row px-md-5' style={{marginTop:'100px',}}>
        <h4 className='section-heading'>Top Selling Products For You</h4>
        <div className="row mt-3 g-3">
          {/* Map over topSellingProducts array and render ProductCard for each product */}
          {topSellingProducts.map(product => (
              <ProductCard 
                key={product.id}
                productImg={product.productImg}
                productName={product.productName}
                sellerName={product.sellername}
                productDes={product.productDes}
                productPrice={product.productPrice}
                availiableStatus={product.availiableStatus}
                prouductTotal={product.prouductTotal}
              />
          ))}
        </div>
      </section>

      {/* Top sellers Section */}
      <section className='row px-md-5 py-4' style={{marginTop:'100px',background:'#eee'}}>
       <h4 className='section-heading'>Top Sellers</h4>
       <div className="mt-4 top-seller-container d-flex gap-3" style={{ overflowX: 'auto', width: '100%' }}>
        {/* Profile Card starts---- use map function fetch in it*/}
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link  to="/shopdetails" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
         {/* Profile Card end---- romove below carts once you fetch the profile detail from server. it is just to display ui */}
         {/* remove ---start */}
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link to="/shopdetails" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link to="/shopdetails" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link  to="/shopdetails" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link to="#" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link to="#" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
        <div className="card position-relative" style={{ flex: '0 0 auto', width: '16rem' }}>
            <img 
            style={{ height: '5rem', objectFit: 'cover', objectPosition: 'center' }}
              src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              className="card-img-top"
              alt="backgroundImg"/>
            <img style={{width:'90px',height:'90px',top:'4rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle" src="https://image.cnbcfm.com/api/v1/image/106347675-1579665885541gettyimages-957028682.jpeg?v=1579665976&w=1920&h=1080" alt="profileImg" />

            <div className="card-body mt-4 px-4 pb-5">
              <h5 className="card-title" style={{fontSize:'16px',fontWeight:'600'}}>Tshongpoen Tenzin Om</h5>
              <p className="card-text" style={{fontSize:'14px',color:'#000'}}>From Punakha</p>
              <Link to="#" className="btn btn-primary d-block mx-auto">View Shop</Link>
            </div>
        </div>
        {/* remove ---end */}
       </div>
      </section>

      {/* Vision-- mission --section */}
      <section className='row px-md-5' style={{marginTop:'100px',}}>
        <h1 className='text-center text-mission'>Empowering businesses through Local trade</h1>
          <p className='text-center sub-text-mission mx-auto w-75 px-md-5'>
            Discover Bhutanese craftsmanship at UnityCrafts! Local businesses can effortlessly sell with 
            a business account, while customers enjoy easy ordering through a simple account setup.
            Elevate your shopping experience with a click at UnityCrafts, connecting buyers and local 
            sellers seamlessly.
          </p>
        <div className="row g-3 mt-4" style={{marginBottom:'100px'}}>
           <div className="col-md-6" style={{height:'440px'}}>
              <div className='h-100 rounded-2 mission d-flex align-items-end p-5'>
                 <div>
                    <h6 className='text-mission-heading'>OUR MISSION</h6>
                    <h4 className='text-mission-sub-heading'>Unite Bhutan's businesses and customers.</h4>
                 </div>
              </div>
           </div>
           <div className="col-md-6 d-flex flex-column gap-3" style={{height:'440px'}}>
              <div className='h-50 rounded-2 location  d-flex align-items-end p-5'>
                <div>
                    <h6 className='location-heading'>OUR LOCATION</h6>
                    <h4 className='location-sub-heading'>We have teams around Bhutan.</h4>
                 </div>
              </div>
              <div className='h-50 rounded-2 promise  d-flex align-items-end p-5'>
                <div>
                    <h6 className='text-mission-heading'>OUR PROMISE</h6>
                    <h4 className='text-mission-sub-heading'>Seamless shopping satisfaction</h4>
                 </div>
              </div>
           </div>
        </div>
      </section>

    </GuestLayout>
  );
};

export default Home;
