import React from 'react'
import GuestLayout from '../layouts/GuestLayout'
import ProductCard from '../components/ProductCard';
import "./../css/shopdetails.css"



const ViewShopDetails = () => {
    const topSellingProducts = [
        { 
          id: 1,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
        { 
          id: 2,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
        { 
          id: 3,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
        { 
          id: 4,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
      ];
  return (
    <GuestLayout>
        <div className='row shopdetail-herobanner mb-3 px-md-5 py-4 gy-2 gx-4 align-items-md-center'>
            <div className="col-md-6">
                <div style={{minHeight:'120px'}} className='bg-body-tertiary bg-opacity-50 d-flex gap-4 p-3 rounded'>
                    <img style={{height:'80px',width:'100px',objectFit:'cover',objectPosition:'top'}} className='img-fluid rounded' src="https://pics.craiyon.com/2023-12-05/dUIhoolxT0qF1sdtpEGq7w.webp" alt="" />
                    <div>
                        <h4 className='fw-bold'>Tenzin Om Tshongkha</h4>
                        <h6>Punakha</h6>
                        <h6>+975 17337526</h6>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <div style={{minHeight:'120px'}} className='bg-body-tertiary bg-opacity-50 rounded'>
                  <div className='d-flex gap-2 p-3 flex-wrap'>
                  <button className='btn btn-dark'>All</button>
                    <button className='btn btn-outline-dark'>Painting Thangka</button>
                    <button className='btn btn-outline-dark'>Bamboo</button>
                    <button className='btn btn-outline-dark'>Woodwork</button>
                    <button className='btn btn-outline-dark'>Bamboo</button>
                    <button className='btn btn-outline-dark'>Painting Thangka</button>
                    <button className='btn btn-outline-dark'>Painting Thangka</button>
                    <button className='btn btn-outline-dark'>Painting Thangka</button>
                  </div>

                </div>
            </div>
        </div>
        <div className="row mt-3 g-3 mb-5 px-md-5">
          {/* Map over topSellingProducts array and render ProductCard for each product */}
          {topSellingProducts.map(product => (
              <ProductCard 
                key={product.productId}
                productImg={product.productImg}
                productName={product.productName}
                sellerName={product.sellername}
                productDes={product.productDes}
                productPrice={product.productPrice}
                availiableStatus={product.availiableStatus}
                prouductTotal={product.prouductTotal}
              />
          ))}
        </div>
    </GuestLayout>
  )
}

export default ViewShopDetails