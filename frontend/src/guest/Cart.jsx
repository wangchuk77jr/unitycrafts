import React, { useState } from 'react';
import GuestLayout from "../layouts/GuestLayout"
import "./../css/cart.css"
import carImage from "./../images/profile.png"
import scan from "./../images/scan.png"

const Cart = () => {

    const [showOverlay, setShowOverlay] = useState(false);
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [showOverlay1, setShowOverlay1] = useState(false);

    const handleDelete = (id) => {
        // setSelectedId(id);
        setShowOverlay(true);
    };

    const handleConfirmDelete = () => {
        // const newData = data.filter(item => item.id !== selectedId);
        // Update your state or perform other actions as needed
        setShowOverlay(false);
        setShowSuccessMessage(true);
        setTimeout(() => {
            setShowSuccessMessage(false);
        }, 2000); // Hide success message after 2 seconds
    };

    const handleCancelDelete = () => {
        setShowOverlay(false);
    };


    const pay = () => {
        setShowOverlay1(true);
    }

    const handleCancel = () => {
        setShowOverlay1(false);
    }
    // Step 1: Set Up State
    const [products, setProducts] = useState([
        { id: 1, name: "Custom Puffer Jacket", price: 8900, quantity: 1, orderStatus: "Pending", shippingStatus: "Packing" },
        { id: 2, name: "Another Product", price: 12000, quantity: 1, orderStatus: "Pending", shippingStatus: "Packing" },
        { id: 3, name: "Custom Puffer Jacket", price: 8900, quantity: 1, orderStatus: "Pending", shippingStatus: "Packing" },
        { id: 4, name: "Another Product", price: 12000, quantity: 1, orderStatus: "Pending", shippingStatus: "Packing" },
        // Add more products as needed
    ]);

    // Step 2: Implement Increment and Decrement Functions
    const incrementQuantity = (productId) => {
        setProducts(prevProducts => {
            return prevProducts.map(product => {
                if (product.id === productId) {
                    return { ...product, quantity: product.quantity + 1 };
                }
                return product;
            });
        });
    };

    const decrementQuantity = (productId) => {
        setProducts(prevProducts => {
            return prevProducts.map(product => {
                if (product.id === productId && product.quantity > 1) {
                    return { ...product, quantity: product.quantity - 1 };
                }
                return product;
            });
        });
    };

    // Step 3: Update UI
    return (
        <GuestLayout>
            <div className="cart-con">
                <h4>CARD DETAILS</h4>
                <div className="car-details">
                    <table>
                        <thead>
                            <tr>
                                <th>Products</th>
                                <th>Quantity</th>
                                <th>Price (NU)</th>
                                <th>Total (NU)</th>
                                <th>Order_Status</th>
                                <th>Action</th>
                                <th>Shipping_Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map(product => (
                                <tr key={product.id}>
                                    <td className='pro'>
                                        <img style={{ paddingBottom: "10px" }} src={carImage} alt="Car" />
                                        <p>{product.name}</p>
                                    </td>
                                    <td>
                                        <div className='num'>
                                            <button className='bx' onClick={() => incrementQuantity(product.id)}>+</button>
                                            <p style={{ paddingTop: "12px" }}>{product.quantity}</p>
                                            <button className='bx' onClick={() => decrementQuantity(product.id)}>-</button>
                                        </div>
                                    </td>
                                    <td>{product.price}</td>
                                    <td>{product.quantity * product.price}</td>
                                    <td>{product.orderStatus}</td>
                                    <td>
                                        <p onClick={pay} className='pay-opt'>Pay</p>
                                    </td>
                                    <td>
                                        {product.shippingStatus}
                                    </td>
                                    <td>

                                        <i onClick={handleDelete} className="fa-solid fa-trash-can trash"></i>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                {showOverlay && (
                    <div className="overlay">
                        <div className="overlay-content">
                            <h2 style={{ fontWeight: "bold" }}>DELETE ITEM</h2>
                            <h5>Are you sure you want to delete an Item?</h5>
                            <div className='btbt'>
                                <button className="bt" onClick={handleCancelDelete}>CANCEL</button>
                                <button style={{ background: "#996418" }} className="bt" onClick={handleConfirmDelete}>YES</button>
                            </div>
                        </div>
                    </div>
                )}
                {showSuccessMessage && (
                    <div className="success-message">
                        <p>You have successfully deleted an Item!</p>
                    </div>
                )}

                {showOverlay1 && (
                    <div className="overlay">
                        <div style={{height:"70%", display:"flex",flexDirection:"column",alignItems:"flex-end"}} className="overlay-content">
                            <i onClick={handleCancel} class="fa-solid fa-xmark"></i>
                            <div className='Over'>
                                <div className='Over'>
                                    <img src={scan} alt="scanImg" />
                                    <p>Nim Dorji</p>
                                </div>
                                <div style={{display:"flex",flexDirection:"column",alignItems:"center"}}>
                                    <input className='inputj' type="number" placeholder='Journal Number' />
                                    <input style={{border:"none",borderBottom:"2px solid black"}} className='inputj' type="text1" placeholder='Address' />
                                    <button className='b' type="submit">Confirm</button>
                                    <li style={{fontSize:"12px",paddingTop:"5px"}}>Thank you for your payment. 
                                        A confirmation will be issued upon successful verification. 
                                        We appreciate your patience during this process. 
                                        Should any issues persist, please feel free to 
                                        contact us via phone for prompt assistance.</li>
                                </div>
                            </div>
                        </div>
                    </div>
                )}

            </div>
        </GuestLayout>
    )
}

export default Cart;
