import React from 'react'
import GuestLayout from '../layouts/GuestLayout'
import about1 from '../images/about1.png';
import about2 from '../images/about2.png';
import about3 from '../images/about3.png';
import about4 from '../images/about4.png';
const About = () => {
  return (
    <GuestLayout>
        <div className="row px-md-5 mt-3 g-4">
            <div className="col-md-6">
                <div style={{minHeight:'100px'}} className='mb-3'>
                    <h1 className='fw-bolder'>Carfting Bhutan's Beauty</h1>
                    <h5>A Tapestry of Tradition and Artistry.</h5>
                </div>
                <div style={{height:'60vh',}}>
                    <img style={{objectFit:'cover',objectPosition:'center'}} className='img-fluid h-100 w-100 rounded-1' src={about1} alt="" />
                </div>
            </div>
            <div className="col-md-6">
                <div style={{minHeight:'100px'}} className='mb-3'>
                    <p>
                        Welcome to UnityCrafts, where Bhutan's vibrant heritage comes to life through timeless 
                        crafts. Explore Thangka paintings, woodwork, textile weaving,and more, each a testament
                        to Bhutanese culture. Join us in preserving tradition and celebrating artistry.
                    </p>
                </div>
                <div className="row g-2" style={{height:'55vh',}}>
                    <div className="col">
                        <img style={{objectFit:'cover',objectPosition:'center'}} className='w-100 h-100 rounded-1' src={about2} alt="" />
                    </div>
                    <div className="col">
                        <img style={{objectFit:'cover',objectPosition:'center'}} className='w-100 h-100 rounded-1' src={about3} alt="" />
                    </div>
                </div>
                <p className='d-flex align-items-end mt-2' style={{height:'5vh',}}>
                    <span className='p-0'>
                        Explore Shop
                        <svg className='ps-3' xmlns="http://www.w3.org/2000/svg" width="91" height="8" viewBox="0 0 91 8" fill="none">
                            <path d="M90.3643 4.35355C90.5596 4.15829 90.5596 3.84171 90.3643 3.64645L87.1823 0.464466C86.9871 0.269204 86.6705 0.269204 86.4752 0.464466C86.28 0.659728 86.28 0.976311 86.4752 1.17157L89.3036 4L86.4752 6.82843C86.28 7.02369 86.28 7.34027 86.4752 7.53553C86.6705 7.7308 86.9871 7.7308 87.1823 7.53553L90.3643 4.35355ZM0.988281 4.5H90.0107V3.5L0.988281 3.5V4.5Z" fill="black"/>
                        </svg>
                    </span>
                </p>
            </div>
        </div>
        <div className="row px-md-5 mt-4 mb-5">
            <div className="col-md-6">
                <h1 className='fw-bolder mb-3'>Our Mission and Values</h1>
                <p>
                    Our mission at UnityCrafts is to preserve and promote Bhutan's rich cultural 
                    heritage by showcasing its diverse array of traditional arts and crafts, fostering
                    appreciation, and providing a platform for artisans to thrive
                </p>
                <ol>
                    <li className='mb-3'>
                        <strong>Tradition: </strong>
                        We uphold the rich cultural traditions of Bhutan, honoring the legacy
                        passed down through generations of artisans.
                    </li>
                    <li className='mb-3'>
                        <strong>Tradition: </strong>
                        We value meticulous attention to detail and dedication to 
                        excellence in every piece crafted, ensuring the highest quality standards.
                    </li>
                    <li>
                        <strong>Tradition: </strong>
                        We are committed to preserving Bhutan's unique heritage
                        by safeguarding traditional arts and crafts and promoting their continued practice.
                    </li>
                </ol>
            </div>
            <div className="col-md-6">
                <img style={{objectFit:'cover',objectPosition:'center'}} className='w-100 rounded-1' src={about4} alt="" />
            </div>
        </div>
    </GuestLayout>
  )
}

export default About