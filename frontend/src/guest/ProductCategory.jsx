import React from 'react';
import GuestLayout from '../layouts/GuestLayout';
import ProductCard from '../components/ProductCard';
import "./../css/shopdetails.css"


const ProductCategory = () => {
    const topSellingProducts = [
        { 
          id: 1,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
        { 
          id: 2,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
        { 
          id: 3,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
        { 
          id: 4,
          productImg: 'https://i0.wp.com/tasteofbhutan.com/wp-content/uploads/2022/02/Tsharzo-the-art-of-bamboo-weaving.jpeg?fit=1024%2C685&ssl=1',
          productName: 'Bangchung',
          sellername: 'Tenzin Om', 
          productDes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc suscipit ut nisl id dapibus.',
          productPrice: 200,
          availiableStatus: 'available',
          prouductTotal: 18,
        },
      ];
  return (
    <GuestLayout>
        <div className='row shopdetail-herobanner mb-3 px-md-5 py-4 gy-2 gx-4 align-items-md-center'>
            <h1 className='text-white text-center fw-bolder text-capitalize'>
                thangka painting
            </h1>
        </div>
        <div className="row mt-3 g-3 mb-5 px-md-5">
          {/* Map over topSellingProducts array and render ProductCard for each product */}
          {topSellingProducts.map(product => (
              <ProductCard 
                key={product.productId}
                productImg={product.productImg}
                productName={product.productName}
                sellerName={product.sellername}
                productDes={product.productDes}
                productPrice={product.productPrice}
                availiableStatus={product.availiableStatus}
                prouductTotal={product.prouductTotal}
              />
          ))}
        </div>
    </GuestLayout>
  )
}

export default ProductCategory