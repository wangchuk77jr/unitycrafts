import React from 'react';
import GuestLayout from '../layouts/GuestLayout';
import bgImage from '../images/contactUs.png'; 
import iconPhone from '../images/mobile-phone 1.png'
import iconLocation from '../images/locationIcon.png'
import "./../css/conatact.css"


const ContactUs = () => {
    return (
        <GuestLayout>
            <div className='row min-vh-100 px-md-5 contact-main-container' >
                <div className="col">
                    <h1 className='mt-4 text-center text-white fw-bolder w-100'>Contact Us</h1>
                    <div className="row rounded-3 mt-4" style={{minHeight:'70vh',background:'rgba(217, 217, 217, 0.66)',}}>
                        <div className="col-md-7">
                            <div className='h-100 border-end d-flex justify-content-between gap-3 p-5'>
                                <div className='text-center w-50'>
                                    <div>
                                        <img className='img-fluid' src={iconPhone} alt="icon" />
                                    </div>
                                    <h2 style={{color:'#543B16'}} className='fw-bold my-3'>By Phone</h2>
                                    <div className='text-center ps-md-3'>
                                        <h6>[ Monday - Friday 9am to 5pm ]</h6>
                                        <h6 className='mt-3 fw-bold'>Phone Number : +975 77868005</h6>
                                        <h6 className='mt-3 fw-bold'>Toll Free : 978-675-888-00000</h6>
                                    </div>
                                </div>
                                <div className='w-50 text-center'>
                                    <div>
                                        <img className='img-fluid' src={iconLocation} alt="icon" />
                                    </div>
                                    <h2 style={{color:'#543B16'}} className='fw-bold my-3'>Location</h2>
                                        <h6>[ Visit our company ]</h6>
                                        <h6 className='fw-bold mt-3'>Kabesa, Thimphu</h6>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-5">
                            <div className='p-5'>
                                <form action="#">
                                    <div class="form-floating  mb-3">
                                        <input  type="email" class="form-control border-primary" id="email" placeholder="name@example.com"/>
                                        <label for="email">Email address</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input type="password" class="form-control border-primary" id="subject" placeholder="Password"/>
                                        <label for="subject">Subject</label>
                                    </div>
                                    <div class="form-floating">
                                        <textarea class="form-control border-primary" placeholder="Leave a feedback here" id="feedback" style={{height: '100px'}}></textarea>
                                        <label for="feedback">Feedback</label>
                                    </div>
                                    <button className='w-100 btn btn-primary p-3 mt-4'>SEND</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </GuestLayout>
    );
}

export default ContactUs;
